public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World");
    }

    public void hello(int a, int b, int c, int d, int e, int f, int j, int k)
    {
        System.out.println("Here's a little function.");
    }

    public void throwsNPE() {
        final String s = null;
        System.out.println(s.equals("abc"));
    }
}
